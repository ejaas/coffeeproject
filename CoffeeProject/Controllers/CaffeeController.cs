﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CoffeeProject.Models;
using CoffeeProject.ViewModel;

namespace CoffeeProject.Controllers
{
    public class CaffeeController : Controller
    {
        // GET: Caffee   /Coffee/Random
        public ActionResult Random()
        {
            var caffee = new Caffee() { name = "Expresso" };
            var customers = new List<Customer>
            {
                new Customer { Name= "Customer 1"},
                new Customer { Name= "Customer 2"}
            };

            var viewModel = new RandomCaffeeViewModel
            {
                Caffee = caffee,
                Customers = customers
            };

            return View(viewModel);
            //  return Content("Hello!");
            // return new EmptyResult();
            // return RedirectToAction("Index", "Home", new { page = 1, sortBy = "name" });
        }

        public ActionResult Edit(int id)
        {
            return Content("id= " + id);

        }

        //caffee

        public ActionResult Index(int? pageIndex, string sortBy)
        {
            if (!pageIndex.HasValue)
                pageIndex = 1;
            if (string.IsNullOrWhiteSpace(sortBy))
                sortBy = "Name";

            return Content(string.Format("pageIndex={0}&sortBy={1}", pageIndex, sortBy));

        }

        [Route("coffee/released/{id}/{price:regex(\\d{4}):range(1, 12)}")]
        public ActionResult ByReleasePrice(int id, int price)
        {
            return Content(id + "/" + price);
        }
            }
}