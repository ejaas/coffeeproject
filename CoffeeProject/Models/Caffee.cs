﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;


namespace CoffeeProject.Models
{
	public class Caffee
	{
		public int id { get; set; }
		public string name { get; set; }

		public string type { get; set; }
		public float price { get; set; }
		public string roast { get; set; }
		public string country { get; set; }
		public string image { get; set; }

		public string revies { get; set; }


	}
}